export const CONSTANS:{BASE_URL:string} = {
    BASE_URL: "https://test-chat-be.herokuapp.com/graphql"
}

interface IDate {
    MINUTES:number
    HOURS:number
    YEAR:number
    DAY:number
    MONTH:number
}

export const DATE:IDate = {
    MINUTES : new Date().getMinutes(),
    HOURS: new Date().getHours(),
    YEAR: new Date().getFullYear(),
    DAY: new Date().getDay(),
    MONTH : new Date().getMonth(),
}

