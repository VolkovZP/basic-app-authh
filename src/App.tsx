import Input from "./components/Input";
import Button from "./components/Button";
import Avatar from "./components/Avatar"
import Dialog from "./components/Dialog";
import './App.sass';

let textInputOrBtn:string = 'Something'

function App() {
  return (
  <>
        <Input placeholder={textInputOrBtn} isValid={false} />
      <Input placeholder={textInputOrBtn} isValid={true} />
      
        <Button color='scondary'types='submit'>{textInputOrBtn}</Button>
      <Button color='primary' types='submit'>{textInputOrBtn}</Button>

      <div className='avatarContainer'>
      <Avatar min={true} color='primary' name='FL' /> 
      <Avatar color='bisc' name='FL'/> 
      </div>

      
      <div className='containerDialogs'>
      <Dialog color="primary" avatarColor='bisc'>
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
      </Dialog>
    <Dialog color="scondary" avatarColor='orange' notMine >
          Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        </Dialog>
      </div>
  </>
  );
}

export default App;
 