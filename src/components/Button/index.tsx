import React from 'react'
import style from './Button.module.sass'
import cx from 'classnames';

type typeBtn = {
    children: React.ReactNode,
    types: 'submit' | 'reset' | 'button' | undefined,
    color?:string
}



export default function Button({ color, types, children }: typeBtn) {
    
    const btnColorStyle = cx(style.btn, {
           [style.scondaryColor]: color === 'scondary',
           [style.primaryColor]: color === 'primary',
           [style.biscColor]: color === 'bisc',
           [style.orangeColor]: color === 'orange'
    })
    

    return (
        <button type={types} className={btnColorStyle}>
            {children}
        </button>
    );
}