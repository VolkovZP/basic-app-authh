import React from 'react'
import style from './Dialog.module.sass'
import cx from 'classnames';
import Avatar from '../Avatar';
import { DATE } from '../../CONSTANTS';
const { MINUTES, HOURS, YEAR, DAY, MONTH } = DATE

interface IDialog  {
    color?: string,
    children: React.ReactNode,
    notMine?: boolean
    avatarColor:string,
}

export default function Dialog({ color, notMine, avatarColor, children }: IDialog) {
    
    const avatarStyle = cx(style.message, {
            [style.scondaryColor]: color === 'scondary',
            [style.primaryColor]: color === 'primary',
            [style.biscColor]: color === 'bisc',
            [style.orangeColor]: color === 'orange',
            [style.transform]:notMine
        })

    return (
        <div className={cx(style.wrapper,
        {
       [style.transform]:notMine
        })}>
            <Avatar min={true} color={avatarColor} name='FL' notMine={notMine} />
        <div className={avatarStyle}>
                {children}
            </div>
            <span className={cx(style.time, { [style.timeNotMine]: notMine })}>
                {!notMine ?
                `${DAY}.${MONTH}.${YEAR} ${HOURS}:${MINUTES}`
                : `${HOURS}:${MINUTES}`}
            </span>
 </div>
    )
}
