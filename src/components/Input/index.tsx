import React,{FunctionComponent} from 'react'
import style from './Input.module.sass'
import cx from 'classnames';

interface IinputComponent {
    placeholder: string;
    isValid?: boolean;
}

function Input({ placeholder, isValid }:IinputComponent) {
    const valid = cx(style.input, {
        [style.inputError]: isValid === true,
        [style.inputNormal]: isValid === false
    })
    
    return (
        <>
            <label className={style.label}>input</label>
            <input className={valid} placeholder={placeholder} />
            {isValid && <span className={style.textError}>Error text</span>}
        </>

    )
}
export default Input