import React from 'react'
import style from './Avatar.module.sass'
import cx from 'classnames';

interface typeBtn  {
    color: string,
    min?:boolean,
    name: string,
    notMine?:boolean
}


export default function Avatar({ color, min, name, notMine }: typeBtn) {
    
const avatarStyle = cx(style.conteiner,
        {
        [style.scondaryColor]: color === 'scondary',
        [style.primaryColor]: color === 'primary',
        [style.biscColor]: color === 'bisc',
        [style.orangeColor]: color === 'orange',
        [style.minSize]: min,
        [style.transform]:notMine
        })

    return (
        <div className={avatarStyle}>
            <span className={style.initials}>{name}</span>
        </div>
    )
}
